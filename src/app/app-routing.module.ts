import {Inject, LOCALE_ID, NgModule} from '@angular/core';
import {Router, RouterModule, Routes} from '@angular/router';
import {DisclosureComponent} from './disclosure/disclosure.component';
import {DataSecurityComponent} from './data-security/data-security.component';
import {Title} from '@angular/platform-browser';
import {NotFoundComponent} from './not-found/not-found.component';

const appRoutes: Routes = [
  {path: 'legal', component: DisclosureComponent},
  {path: 'dataSecurity', component: DataSecurityComponent},
  {path: '', redirectTo: 'legal', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
}
