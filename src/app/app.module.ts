import {BrowserModule, Title} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {DataSecurityComponent} from './data-security/data-security.component';
import {DisclosureComponent} from './disclosure/disclosure.component';
import {AppRoutingModule} from './app-routing.module';
import {FooterComponent} from './footer/footer.component';
import {ContactFormComponent} from './contact-form/contact-form.component';
import {LanguageSwitchComponent} from './language-switch/language-switch.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DataSecurityComponent,
    DisclosureComponent,
    FooterComponent,
    ContactFormComponent,
    LanguageSwitchComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})

export class AppModule {
}
