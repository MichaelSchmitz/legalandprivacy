import {Component, OnInit} from '@angular/core';
import {LanguageService, TitleVal} from '../language.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html'
})
export class NotFoundComponent implements OnInit {

  constructor(private language: LanguageService) {
  }

  ngOnInit() {
    this.language.setTitle(new TitleVal('Not Found', 'Nicht gefunden'));
  }
}
