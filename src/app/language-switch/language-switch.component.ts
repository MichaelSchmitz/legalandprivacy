import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../language.service';

@Component({
  selector: 'app-language-switch',
  templateUrl: './language-switch.component.html'
})
export class LanguageSwitchComponent implements OnInit {

  constructor(public language: LanguageService) {
  }

  ngOnInit() {
  }
}
