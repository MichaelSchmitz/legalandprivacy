import {Component, OnInit} from '@angular/core';
import {LanguageService, TitleVal} from '../language.service';

@Component({
  selector: 'app-data-security',
  templateUrl: './data-security.component.html'
})
export class DataSecurityComponent implements OnInit {

  constructor(private language: LanguageService) {
  }

  ngOnInit() {
    this.language.setTitle(new TitleVal('Data Protection Policy', 'Datenschutzerklärung'));
  }
}
