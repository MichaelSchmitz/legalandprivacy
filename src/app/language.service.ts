import {Inject, Injectable, LOCALE_ID} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor(private titleService: Title, @Inject(LOCALE_ID) protected localeId: string, private router: Router) {
  }

  setTitle(title: TitleVal): void {
    if (this.localeId === 'de') {
      this.titleService.setTitle(title.de);
    } else if (this.localeId === 'en-US') {
      this.titleService.setTitle(title.en);
    }
  }

  getUrl(): string {
    return this.router.url;
  }

  getLanguage(): string {
    let tmp: string;
    if (this.localeId === 'en-US') {
      tmp = 'en';
    } else {
      tmp = this.localeId;
    }
    return tmp;
  }
}

export class TitleVal {
  readonly en: string;
  readonly de: string;

  public constructor(en: string, de: string) {
    this.en = en;
    this.de = de;
  }
}
