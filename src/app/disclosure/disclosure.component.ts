import {Component, OnInit} from '@angular/core';
import {LanguageService, TitleVal} from '../language.service';

@Component({
  selector: 'app-disclosure',
  templateUrl: './disclosure.component.html'
})
export class DisclosureComponent implements OnInit {

  constructor(private language: LanguageService) {
  }

  ngOnInit() {
    this.language.setTitle(new TitleVal('Legal Notice', 'Impressum'));
  }
}
